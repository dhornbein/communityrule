---
layout: rule
title: Circles
permalink: /templates/circles/
icon: /assets/tabler_icons/circle.svg

community-name: 

# BASICS
structure: Activities are carried out through individual Roles and small groups called Circles, which have the ability to decide and act on matters in their domain. Representatives of Circles meet in a Council to coordinate efforts and determine the domains of each Circle.
mission:
values: The purpose of this structure is to empower individuals and small groups to act in their domains of expertise in ways that are accountable to the larger group.
legal: 

# PARTICIPANTS
membership: To join the community, a person must request membership in a particular Circle, and the Circle's members can respond as they see fit.
removal:
roles: The Circle can create Roles and assign them authority over specified sub-domains. Each Circle should have one Role that represents it on the Council. Members can hold more than one Role within or across Circles.
limits: Circles should rotate their representative to the Council at each Council meeting so that every member of the Circle has an equal opportunity to participate.

# POLICY
rights: 
decision: Policies are determined by consent within the Circle assigned to the relevant domain. Consent means that no more than one-tenth of the relevant group presents a serious objection to a policy. Circles may be self-organized among members, provided they do not overlap with the domains of other Circles. In a Council, changes may be made by consent over the Circles' domains. By consent, Circles may also establish Roles, modify them, and appoint members to them. Role-holders are understood to have authority over their Roles' sub-domains, but they are expected to consult with anyone affected by their decisions.
implementation: Circles, as well as individual Role-holders, are responsible for implementing the decisions they make over their respective domains.
oversight: Circles should evaluate the actions of the Role-holders within them, and the Council should evaluate the Circles within it.

# PROCESS
access:
economics: 
deliberation: Circles and the Council should establish regular meetings at which they coordinate their activities and adjudicate conflicts over domains.
grievances: 

# EVOLUTION
records: 
modification: The Council may modify this Rule by consent.
---

Use this template as-is or edit it to adapt it to your community.

Based on [sociocracy](https://en.wikipedia.org/wiki/Sociocracy) and [affinity group](https://en.wikipedia.org/wiki/Affinity_group) methods.
