---
layout: module
title: Range Voting
permalink: /modules/range_voting/
summary: Voters score each option within a range, and the option with the highest average score wins.
type: decision
---

