---
layout: module
title: Meritocracy
permalink: /modules/meritocracy/
summary: Participants gain authority according to their excellence by shared standards.
type: process
---
