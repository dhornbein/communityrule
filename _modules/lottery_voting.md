---
layout: module
title: Lottery Voting
permalink: /modules/lottery_voting/
summary: Decision makers are chosen at random from among the community.
type: process
---

Lottery voting is a system that strives to elect a legislature that accurately represents a community. Each citizen casts a ballot and one from each district is randomly drawn from the pool.

**Input:** voting populace; equally divided districts; equal-value ballots from each voter; candidate tickets

**Output:** an elected population based on probability

## Background

Lottery voting was conceived by Akhil Reed Amar in a 1984 Yale Law Journal article. He followed this with a 1995 piece that classifies the system as a thought experiment and a way to better understand the pros and cons of current voting systems and their democratic mechanisms.

## Feedback loops

### Sensitivities

* Lottery voting has the potential to more fairly represent minority groups; a candidate with only 20% of the votes still has a 20% chance of being chosen randomly
* Every candidate has a feasible chance of winning, which [can lead to greater voter turnout]( https://cedar.wwu.edu/cgi/viewcontent.cgi?article=1042&context=wwu_honors)
* It can prevent strategic voting in which an individual votes for someone they don’t support as strongly to prevent another candidate from winning
* Gerrymandering would not be effective nor useful in this system; voting districts would be equal in population and therefore ballots would be equal in representation 
* Challenges elected officials staying in office for exorbitant amounts of time, as there is a higher chance that a new candidate will be elected; Amar defines this as a type of built-in term limit

### Oversights

* This system is only effective on a large scale with multiple representatives
* The chance of an undesirable candidate getting chosen from the pool is still present
* It can decrease the voting populace’s feelings of political efficacy, or having a direct impact on the outcome

## Implementations

### Communities

Because of its original conception as a thought experiment, lottery voting is not currently implemented in any large-scale democracies.

### Tools

* Probability tools can be found [online]( https://www.interactive-maths.com/probability-tools-flash.html) and in a variety of [phone applications]( https://apps.apple.com/us/app/probability-tools/id544340525).


## Further resources

Amar, A. R. (1995). Lottery Voting: A Thought Experiment. In University of Chicago Legal Forum (Vol. 1995, No. 1, p. 8).

Amar, A. (1984). Choosing Representatives by Lottery Voting. The Yale Law Journal, 93(7), 1283-1308. doi:10.2307/796258

GUERRERO, A. A. (2014). Against elections: The lottocratic alternative. Philosophy & Public Affairs, 42(2), 135-178. doi:10.1111/papa.12029
