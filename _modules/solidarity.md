---
layout: module
title: Solidarity
permalink: /modules/solidarity/
summary: A commitment to mutual support infuses collection action.
type: culture
---

<!--
* Workers organizations
* Party whips, enforcing party discipline
-->
