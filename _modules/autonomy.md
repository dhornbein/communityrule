---
layout: module
title: Autonomy
permalink: /modules/autonomy/
summary: A high value is placed on self-determination among individuals or groups.
type: culture
---
